# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from ..data_policies import set_data_policy


@pytest.fixture
def esrf_data_policy(session, icat_backend):
    set_data_policy(session, "esrf")


@pytest.fixture
def esrf_data_policy_tango(session, icat_tango_backend):
    set_data_policy(session, "esrf")


@pytest.fixture
def session2(beacon, scan_tmpdir):
    session = beacon.get("test_session2")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    yield session
    session.close()


@pytest.fixture
def esrf_data_policy2(session2, icat_backend):
    set_data_policy(session2, "esrf")


@pytest.fixture
def esrf_data_policy2_tango(session2, icat_tango_backend):
    set_data_policy(session2, "esrf")
