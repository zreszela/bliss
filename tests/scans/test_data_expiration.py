import pytest
import gevent
from bliss.common.scans import loopscan
from bliss.scanning.group import Sequence
from bliss.scanning.group import Group
from ..data_policies import set_data_policy


@pytest.fixture
def esrf_session(session, icat_backend):
    set_data_policy(session, "esrf")
    session.scan_saving.writer = "null"
    yield session


@pytest.fixture
def esrf_writer_session(session, icat_backend, nexus_writer_service):
    set_data_policy(session, "esrf")
    session.scan_saving.writer = "nexus"
    yield session


def assert_expiration(redis_data_conn, session_name, db_names, parents_expire=True):
    keys = [db_name.decode() for db_name in redis_data_conn.keys("*")]
    for prefix in db_names:
        compare_ttl = None
        for key in keys:
            if not key.startswith(session_name):
                continue
            ttl = redis_data_conn.ttl(key)
            if key.startswith(prefix):
                # Key associated to the object
                assert ttl > 0, key
            elif prefix.startswith(key):
                # Key associated to a parent of the object
                if parents_expire:
                    assert ttl > 0, key
                else:
                    assert ttl == -1, key
            if ttl == -1:
                continue
            if compare_ttl is None:
                compare_ttl = ttl
            else:
                assert abs(compare_ttl - ttl) < 0.5, key


def normal_scan(session):
    diode = session.env_dict["diode"]
    scan = loopscan(1, 0.1, diode)
    return [scan.node.db_name]


def interrupted_scan(session):
    diode = session.env_dict["diode"]
    scan = loopscan(1000, 0.1, diode, run=False)

    gscan = gevent.spawn(scan.run)
    gevent.sleep(2)
    gscan.kill(KeyboardInterrupt)

    return [scan.node.db_name]


def sequence_scan(session):
    diode = session.env_dict["diode"]
    scan1 = loopscan(3, .1, diode)

    gevent.sleep(1)

    seq = Sequence()
    with seq.sequence_context() as seq_context:
        scan2 = loopscan(3, .05, diode)
        seq_context.add(scan2)
        seq_context.add(scan1)

    return [scan.node.db_name for scan in (seq, scan1, scan2)]


def group_scan(session):
    diode = session.env_dict["diode"]
    scan1 = loopscan(3, .1, diode)
    gevent.sleep(1)

    scan2 = loopscan(3, .05, diode)
    gevent.sleep(1)

    group = Group(scan2, scan1)
    return [scan.node.db_name for scan in (group, scan1, scan2)]


def test_expiration_after_scan(session, redis_data_conn):
    db_names = normal_scan(session)
    assert_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_scan_esrf(esrf_session, redis_data_conn):
    db_names = normal_scan(esrf_session)
    assert_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def test_expiration_after_interrupted_scan(session, redis_data_conn):
    db_names = interrupted_scan(session)
    assert_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_interrupted_scan_esrf(esrf_session, redis_data_conn):
    db_names = interrupted_scan(esrf_session)
    assert_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def test_expiration_after_sequence(session, redis_data_conn):
    db_names = sequence_scan(session)
    assert_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_sequence_esrf(esrf_session, redis_data_conn):
    db_names = sequence_scan(esrf_session)
    assert_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def test_expiration_after_group(session, redis_data_conn):
    db_names = group_scan(session)
    assert_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_group_esrf(esrf_session, redis_data_conn):
    db_names = group_scan(esrf_session)
    assert_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def test_expiration_esrf_dataset(esrf_writer_session, redis_data_conn):
    normal_scan(esrf_writer_session)
    db_names = [esrf_writer_session.scan_saving.dataset.node.db_name]
    esrf_writer_session.scan_saving.newdataset(None)
    assert_expiration(
        redis_data_conn, esrf_writer_session.name, db_names, parents_expire=False
    )


def test_expiration_esrf_proposal(esrf_writer_session, redis_data_conn):
    normal_scan(esrf_writer_session)
    old_proposal = esrf_writer_session.scan_saving.proposal_name
    db_name = esrf_writer_session.scan_saving.proposal.node.db_name
    parts = db_name.split(":")

    for i in range(1, len(parts)):
        key = ":".join(parts[:i])
        assert redis_data_conn.ttl(key) == -1, key

    esrf_writer_session.scan_saving.newproposal("hg123")
    new_db_name = esrf_writer_session.scan_saving.proposal.node.db_name

    for i in range(1, len(parts)):
        key = ":".join(parts[:i])
        ttl = redis_data_conn.ttl(key)
        if new_db_name.startswith(key):
            assert ttl == -1, key
        else:
            assert ttl > 0, key

    esrf_writer_session.scan_saving.newproposal(old_proposal)

    for i in range(1, len(parts)):
        key = ":".join(parts[:i])
        assert redis_data_conn.ttl(key) == -1, key
