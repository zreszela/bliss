from bliss.config.conductor import client


def set_expiration_time(keys, seconds):
    """Set the expiration time of all Redis keys
    """
    async_proxy = client.get_redis_proxy(db=1).pipeline()
    try:
        for name in keys:
            async_proxy.expire(name, seconds)
    finally:
        async_proxy.execute()


def remove_expiration_time(keys):
    """Remove the expiration time of all Redis keys
    """
    async_proxy = client.get_redis_proxy(db=1).pipeline()
    try:
        for name in keys:
            async_proxy.persist(name)
    finally:
        async_proxy.execute()
